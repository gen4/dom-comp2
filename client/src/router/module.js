export default [{
  path: '/admin/users',
  meta: {
    adminOnly: true
  },
  component: (resolve) => {
    import('../view/UserList.vue').then(resolve)
  }
}, {
  path: '/admin/things',
  meta: {
    adminOnly: true
  },
  component: (resolve) => {
    import('../view/ThingList.vue').then(resolve)
  }
}, {
  path: '/admin/voting',
  meta: {
    adminOnly: true
  },
  component: (resolve) => {
    import('../view/Voting.vue').then(resolve)
  }
}, {
  path: '/admin/top',
  meta: {
    adminOnly: true
  },
  component: (resolve) => {
    import('../view/Top.vue').then(resolve)
  }
}, {
  path: '/admin/jury',
  meta: {
    adminOnly: true
  },
  component: (resolve) => {
    import('../view/Jury.vue').then(resolve)
  }
}]
// {
// path: '/dashboard',
// component: (resolve) => {
//   import('../view/Dashboard.vue').then(resolve)
// }
// },
