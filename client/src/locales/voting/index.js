import en from './en'
import ruRU from './ru-RU'

export default {
  'ru-RU': ruRU,
  en
}
