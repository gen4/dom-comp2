export default {
  siteName: 'ДОМ.РФ',
  noData: 'Данные не были предоставлены',
  title: 'Dom Competition 2018',
  constant: {
    name: 'Name',
    desc: 'Description'
  },
  confirm: {
    title: 'Warning',
    yes: 'да',
    no: 'нет',
    good: 'хорошо',
    bad: 'плохо',
    ok: 'save',
    cancel: 'cancel',
    prevStep: 'Previous',
    nextStep: 'Next',
    remove: 'This will remove the selected {content} forever, continue?',
    confirmSelected: 'You have selected the following items. Please confirm your choices as this action can\'t be recoveried'
  },
  label: {
    name: 'Name',
    enable: 'Enable'
  },
  status: {
    enabled: 'Enabled',
    disabled: 'Disabled'
  },
  operation: {
    add: 'Add',
    create: 'Create',
    edit: 'Edit',
    update: 'Update',
    remove: 'Remove',
    multiRemove: 'Multi remove',
    operation: 'Operation',
    search: 'Search',
    enable: 'Click to enable',
    disable: 'Click to disable'
  },
  message: {
    save: {
      ok: 'Saved!',
      err: 'Ошибка записи'
    },
    formErrors: 'Форма заполнена некорректно',
    error: 'Error',
    created: 'Create successed',
    createFailed: 'Create failed',
    updated: 'Update successed',
    updateFailed: 'Update failed',
    removed: 'Delete successed',
    removeFailed: 'Delete failed'
  },
  http: {
    error: {
      E401: 'Нет авторизации ',
      E403: 'Ошибка доступа',
      E404: 'Url не найден',
      E500: 'Ошибка сервера',
      others: 'Возникла ошибка, попробуйте еще раз',
      SID: 'Данный секретный номер (ID) уже используется'
    }
  },
  triradio: {
    all: 'Все'
  },
  vote: {
    bad: 'Плохо',
    good: 'Отлично',
    prev: 'Предыдущий',
    next: 'Следующий'
  },
  from: 'от',
  to: 'до',
  likeDisabled: 'Голосование окончено',
  projectsToVote: 'Осталось оценить проектов',
  countdown: {
    d: 'д',
    h: 'ч',
    m: 'м',
    s: 'с'
  },
  back: 'На главную',
  taptozoom: 'Нажмите для увеличения'
}
