export default {
  thing: {
    projectNumber: 'Project #',
    emptySubproject: 'Mark as unsubmitted',
    details: {
      tabs: {
        common: 'General',
        panel: 'Panels',
        album: 'Model',
        video: 'Video',
        data: 'Technical parameters'
      }
    },
    subs: {
      'sub1': 'Urban block',
      'sub2': 'Building',
      'sub2_l_0': 'Private house',
      'sub2_m_0': 'Urban villa',
      'sub2_h_0': 'Section with central access',
      'sub2_l_1': 'Cluster of private houses',
      'sub2_m_1': 'Section with central access',
      'sub2_h_1': 'Section with gallery access',
      'sub2_l_2': 'Townhouse',
      'sub2_m_2': 'Section with gallery access',
      'sub2_h_2': 'Tower',
      'sub2_l_3': 'Urban villa',
      'sub3': 'Apartment',
      'sub3_0': 'Studio',
      'sub3_1': '1-room',
      'sub3_2': '2-room',
      'sub3_3': '3-room',
      'sub3_4': '4-room'
    },
    breadcrumb: {
      home: 'Homepage***',
      current: 'Competition Proposals'
    },
    tooltip: {
      sub1: {
        area: 'Territory of urban development outlined by the frontage lines of streets, roads and other common areas. Streets are adjacent to the urban block, but not included in its total area',
        height: 'Determines the range of number of storeys in the buildings of the block, not taking landmark object (tower) into account',
        developedPercent: 'The ratio of the footprint area of the buildings located on the territory to the area of the territory itself',
        dens: 'The ratio of the total area of  buildings located on  the territory to  the area of the territory itself',
        commers: 'Рarameter indicating the total area of commercial infrastructure objects compared to the total development area',
        population: 'Amount of residents per 1 ha of territory based on 30 sq. m of housing per capita',
        edu: 'Location of a kindergarten or school in the block according to the Brief'
      },
      sub2: {
        buildingHeight: 'Maximum number of overground storeys in the building',
        liveArea: 'The sum of  the floor areas of  the building measured within the inner surfaces of the outer walls, as well as the areas of the balconies, loggias and open verandas',
        flatArea: 'The sum of  the areas of their premises, built-in wardrobes, as well as loggias, balconies, verandas, terraces and cold storerooms, calculated with lowering factors',
        buildingArea: 'Total area of the residential part plus the sum of the areas of non-residential and technical floors, cellars (basements) and attics measured within the interior surfaces of the external walls, as well as the areas of balconies, loggias and open verandas of these floors',
        floorArea: 'Area of the first overground level of the building',
        k1: 'Ratio of net area to total area of apartments',
        k2: 'Ratio of gross area to total area of apartments',
        k3: 'Ratio of volume of load-bearing structures to total area of apartments',
        k4: 'Ratio of building envelope area to total area of apartments',
        k5: 'Ratio of glazing area to building envelope area',
        m2price: 'Building cost of 1 sq. m of area of apartments'
      }
    },
    items: {
      sub1: {
        area: 'ha',
        liveArea: 'm²',
        length: 'm',
        height: 'storeys',
        developedPercent: '%',
        dens: 'm² / ha',
        commers: '%',
        parking: 'spaces / 1000 ppl',
        population: 'ppl / ha',
        buildingHeight: 'storeys',
        flatArea: 'm²',
        buildingArea: 'm²',
        m2price: '₽',
        floorArea: 'm²',
        flatsQty: 'units',
        elevatorsQty: 'units'
      },
      sub2: {
        area: 'ha',
        liveArea: 'm²',
        length: 'm',
        height: 'storeys',
        developedPercent: '%',
        dens: 'm² / ha',
        commers: '%',
        parking: 'spaces / 1000 ppl',
        population: 'ppl / ha',
        buildingHeight: 'storeys',
        flatArea: 'm²',
        buildingArea: 'm²',
        m2price: '₽',
        floorArea: 'm²',
        flatsQty: 'units',
        elevatorsQty: 'units'
      },
      sub3: {
        area: 'm²',
        liveArea: 'm²',
        length: 'm',
        height: 'storeys',
        developedPercent: '%',
        dens: 'm² / ha',
        commers: '%',
        parking: 'spaces / 1000 ppl',
        population: 'ppl / ha',
        buildingHeight: 'storeys',
        flatArea: 'm²',
        buildingArea: 'm²',
        m2price: '₽',
        floorArea: 'm²',
        flatsQty: 'units',
        elevatorsQty: 'units'
      }
    },
    model: {
      cover: 'Cover',
      pid: 'Public ID (new)',
      sid: 'Hidden ID (old)',
      commonEvaluationFlat: 'Compliance with the Brief: Apartments',
      commonEvaluationBuilding: 'Compliance with the Brief: Buildings',
      commonEvaluationBlock: 'Compliance with the Brief: Urban Block',
      variant: 'Option',
      images: 'Images',
      sub1: {
        commonEvaluation: 'General Compliance with the Brief',
        label: 'Urban block',
        area: 'Urban block area',
        length: 'Urban block length',
        height: 'Building height',
        developedPercent: 'Building footprint',
        dens: 'Urban density',
        commers: 'Proportion of commercial space',
        parking: 'Provision of parking spaces',
        population: 'Population density',
        edu: 'Educational facility'
      },
      sub2: {
        buildingHeight: 'Building height',
        commonEvaluation: 'General Compliance with the Brief',
        label: 'Building',
        l: {
          0: 'Private house',
          1: 'Cluster of private houses',
          2: 'Townhouse',
          3: 'Urban villa'
        },
        m: {
          0: 'Urban villa',
          1: 'Section with central access',
          2: 'Section with gallery access'
        },
        h: {
          0: 'Section with central access',
          1: 'Section with gallery access',
          2: 'Tower'
        },
        liveArea: 'Net area',
        flatArea: 'Total area of apartments',
        buildingArea: 'Gross area',
        k1: 'Index 1',
        k2: 'Index 2',
        k3: 'Index 3',
        k4: 'Index 4',
        k5: 'Index 5',
        m2price: 'Price per 1 m² of apartments',
        floorArea: 'Building footprint',
        flatsQty: 'Number of apartments per floor',
        doubleOriented: 'Apartments with two-side orientation',
        elevatorsQty: 'Number of elevators',
        concierge: 'Concierge room',
        transEntrance: 'Transparent entrance',
        storage: 'Storage space',
        sharedSpaces: 'Shared space'
      },
      sub3: {
        commonEvaluation: 'General Compliance nith the Brief',
        label: 'Apartments',
        0: 'Studio apartment',
        1: '1-room apartment',
        2: '2-room apartment',
        3: '3-room apartment',
        4: '4-room apartment',
        area: 'Area',
        liveArea: 'Living space',
        windows: 'null',
        balcony: 'null',
        balconyGlass: 'null',
        rightShape: 'null'
      },
      avgCost: {
        no: 'Not specified',
        label: 'Cost evaluation',
        l: '₽',
        m: '₽₽',
        h: '₽₽₽'
      },
      commonEvaluation: {
        label: 'General Compliance with the Brief',
        l: 'yes',
        m: 'partly',
        h: 'no'
      },
      envType: {
        label: 'Среда',
        l: 'Low-rise',
        m: 'Mid-rise',
        h: 'Central'
      },
      prizeType: {
        label: 'Winner Type',
        '1': '1st category prize',
        '2': '2nd category prize',
        '3': '3rd category prize'
      },
      peopleFav: 'People\'s choice'
    },
    rules: {
      name: 'Please input the name',
      requiredField: 'Обязательное поле'
    },
    edit: {
      create: 'Добавление работы',
      update: 'Редактирование работы'
    },
    searchByIdPlaceholder: 'Enter a project ID'
  }
}
