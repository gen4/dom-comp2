/**
 * Populate DB with admin user data on server start
 */

'use strict'
var _ = require('lodash')
var Promise = require('bluebird');
var User = require('../api/user/user.model')
var Sid = require('../api/sid/sid.model')
var Checker = require('../api/checker/checker.model')
var Thing = require('../api/thing/thing.model')
var System = require('../api/system/system.model')
var Stage = require('../api/stage/stage.model')
var Like = require('../api/like/like.model')
var ObjectId = require('mongoose').Types.ObjectId;

function doSystem(){
  return System.findOne({})
    .lean()
    .then(function (sys) {
      if(!sys){
        Stage.findOne({})
          .sort('order')
          .lean()
          .then((stage)=>{
            return System.create({currentStage:stage})
          })
      }
      return null
    })
    .then(function(){
      console.log('finished populating system')
      return null;
    })
    .catch(errHandler)
}

function doUsers(){
  return User.findOne({ role: 'admin' })
    .lean()
    .then(function (admin) {
      if (!admin) {
        return User.create({
          provider: 'local',
          role: 'admin',
          name: 'Admin',
          username: 'admin',
          password: 'pinklamp'
        },
          {
            provider: 'local',
            role: 'guest',
            name: 'Guest',
            username: 'guest',
            password: 'kbguest'
          })
      }
      return null
    })
    .then(function(){
        console.log('finished populating users')
        return null;
    })
    .catch(errHandler)
}
// search for admin user, if no, create one


var secretNumbers = ['009020','009112','009116','009156','009246','009256','009272','009291','009303','009329','009330','009368','009454','009463','009654','009705','009764','009896','009910','009918','010036','010109','010158','010165','010208','010267','010344','010406','010540','010555','010591','010617','010626','010635','010709','010832','011215','011326','011672','011767','011908','012073','012170','012213','012428','012562','012677','012772','012919','013056','013188','013682','013744','013847','013854','014084','014175','014450','014518','014578','014632','015159','015236','015610','015658','009074','009135','009233','009290','009320','009347','009730','009834','009963','010234','010702','010724','011138','012316','012552','012589','013023','013290','013314','014023','014553','014681','014907','015575','009036','009543','011272','011481','013865','012010','013261','015370','009019','009029','009054','009087','009105','009129','009144','009181','009240','009270','009274','009304','009345','009366','009369','009445','009485','009490','009495','009566','009743','009970','009983','010113','010219','010271','010479','010533','010537','010647','010657','010687','010695','010885','010899','010913','011061','011107','011144','011288','011905','012035','012068','012126','012128','012239','012333','012338','012344','012719','012797','012953','013148','013350','013356','013505','013531','013725','013933','014080','014166','014185','014357','014389','014425','014491','014646','014658','014669','014699','014759','014844','014868','014880','014910','014929','015063','015107','015245','015247','015351','015379','015448','015506','015557','015578','015591','015595','015615','015624','015655','009184','009185','009226','009261','009403','009591','009943','010373','010465','010927','010941','010969','011224','011882','012095','012100','012774','013194','013307','013535','013781','014177','014220','014643','014715','015060','015200','015285','015312','015613','015647','015671','010244','010285','010691','010745','012346','012829','013093','014193','014668','014815','014982','015485','015666','015677','015396','009046','009127','009207','009289','009336','009374','009498','009514','009924','009992','010038','010167','010241','010365','010441','010460','010496','010582','010604','010633','011281','011401','011641','012190','012383','012850','013060','013090','013186','014276','014325','014428','014885','015164','015309','015516','015530','015560','015561','015657','009123','009209','009554','010171','011183','011438','011631','013566','013611','014354','014356','014453','015398','015649','015674','015676','009660','010095','012242','012339','012857','014617','015325', '009997', '009094'];

secretNumbers = _.map(secretNumbers, function (id) {
  return { _id: id }
});

function doSids(){

  return new Promise(function(resolve, reject){
    Sid.findOne({})
      .lean()
      .then(function(sids){
        // if(sids){
        //   return Promise.reject()
        // }
        return null
      })
      .then(function(){
        return Sid.remove({}).exec()
      })
      .then(function(){
        return Sid.collection.insert(secretNumbers)
      })
      .then(function(){
        console.info('%d sids were successfully stored.', secretNumbers.length);
        return null;
      })
      .catch(errHandler)
      .finally(function(){
        resolve()
      })
  })
}






//   developedPercent: { type: Number, min: 0, max: 100 },
//   dens: { type: Number, min: 0 },
//   open: { type: Number, min: 0, max: 100 },
//   commers: { type: Number, min: 0, max: 100 },
//   parking: { type: Number, min: 0 },
//   population: { type: Number, min: 0 },
//   edu: { type: Boolean },
//   percflat0: { type: Number, min: 0, max: 100 },
//   percflat1: { type: Number, min: 0, max: 100 },
//   percflat2: { type: Number, min: 0, max: 100 },
//   percflat3: { type: Number, min: 0, max: 100 },
//   percflat4: { type: Number, min: 0, max: 100 }
// }, { _id: false })
//
//   var sub2ItemSchema = new Schema({
//     images: [imageSchema],
//     id: { type: String, unique: true, required: true },
//     liveArea: { type: Number, min: 0 },
//     flatArea: { type: Number, min: 0 },
//     buildingArea: { type: Number, min: 0 },
//     baseArea: { type: Number, min: 0 },
//     frontArea: { type: Number, min: 0 },
//     glassArea: { type: Number, min: 0 },
//     k1: { type: Number, min: 0, max: 100 },
//     k2: { type: Number, min: 0, max: 100 },
//     k3: { type: Number, min: 0, max: 100 },
//     k4: { type: Number, min: 0, max: 100 },
//     k5: { type: Number, min: 0, max: 100 },
//     m2price: { type: Number, min: 0 },
//     celHeight: { type: Number, min: 0 },
//     floorArea: { type: Number, min: 0 },
//     flatsQty: { type: Number, min: 0 },
//     doubleOriented: { type: Number, min: 0, max: 100 },
//     drainage: { type: Boolean },
//     elevatorsQty: { type: Number, min: 0 },
//     concierge: { type: Boolean },
//     transEntrance: { type: Boolean },
//     storage: { type: Boolean },
//     sharedSpaces: { type: Boolean }
//   }, { _id: false })
//
//   var sub3ItemSchema = new Schema({
//     images: [imageSchema],
//     id: { type: String, unique: true, required: true },
//     area: { type: Number, min: 0 },
//     liveArea: { type: Number, min: 0 },
//     windows: { type: Boolean },
//     balcony: { type: Boolean },
//     balconyGlass: { type: Boolean },
//     rightShape: { type: Boolean }
//   }, { _id: false })}
var checkers = [
  {
    _id: 'avgCost',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationFlat',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationBuilding',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationBlock',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'envType',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  //  --- --- SUB 1
  {
    _id: 'commonEvaluation',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'area',
    type: 'num',
    grades: {
      l: [1.9, 2, 4, 4.2], //4.2
      m: [1.9, 2, 3, 3.2],
      h: [0.6, 0.6, 1.5, 1.65]
    }
  },
  {
    _id: 'length',
    type: 'num',
    grades: {
      l: [190, 200, 300, 315],
      m: [126, 140, 200, 210],
      h: [75, 75, 150, 165]
    }
  },
  {
    _id: 'height',
    type: 'range',
    grades: {
      l: [1, 1, 4, 4],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'developedPercent',
    type: 'num',
    grades: {
      l: [19, 20, 25, 27],
      m: [22, 25, 30, 35],
      h: [30, 30, 40, 50]
    }
  },
  {
    _id: 'dens',
    type: 'num',
    grades: {
      l: [4000, 4000, 6000, 6300],
      m: [13500, 15000, 20000, 22000],
      h: [17100, 18000, 25000, 26250]
    }
  },
  {
    _id: 'commers',
    type: 'num',
    grades: {
      l: [5, 5, 10, 12],
      m: [5, 5, 20, 22],
      h: [20, 20, 40, 45]
    }
  },
  {
    _id: 'parking',
    type: 'num',
    grades: {
      l: [335, 355, 395, 415],
      m: [212, 223, 247, 259],
      h: [135, 143, 158, 165]
    }
  },
  {
    _id: 'population',
    type: 'num',
    grades: {
      l: [45, 50, 80, 90],
      m: [270, 300, 450, 495],
      h: [360, 400, 450, 500]
    }
  },
  {
    _id: 'edu',
    type: 'bol',
    grades: {
      l: [0.5, 0.5, 1.5, 2],
      m: [0.5, 0.5, 1.5, 2],
      h: [0.5, 0.5, 1.5, 2],
    }
  },


//  --- --- SUB 2


  {
    _id: 'buildingHeight.0',
    type: 'num',
    grades: {
      l: [1, 1, 2, 2],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'buildingHeight.1',
    type: 'num',
    grades: {
      l: [1, 1, 2, 2],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'buildingHeight.2',
    type: 'num',
    grades: {
      l: [1, 1, 2, 3],
      m: [4, 5, 7, 8],
      h: [8, 8, 18, 18]
    }
  },
  {
    _id: 'buildingHeight.3',
    type: 'num',
    grades: {
      l: [2, 2, 4, 4]
    }
  },
  //

  // {
  //   _id: 'k1.0',
  //   type: 'num',
  //   grades: {
  //     l: [1, 1, 9999, 9999],
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  // {
  //   _id: 'k1.1',
  //   type: 'num',
  //   grades: {
  //     l: [1, 1, 9999, 9999],
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  // {
  //   _id: 'k1.2',
  //   type: 'num',
  //   grades: {
  //     l: [1, 1, 9999, 9999],
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  //
  // //
  //
  // {
  //   _id: 'k2.0',
  //   type: 'num',
  //   grades: {
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  // {
  //   _id: 'k2.1',
  //   type: 'num',
  //   grades: {
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  // {
  //   _id: 'k2.2',
  //   type: 'num',
  //   grades: {
  //     m: [1, 1, 9999, 9999],
  //     h: [1, 1, 9999, 9999]
  //   }
  // },
  //
  // //
  // {
  //   _id: 'k5.0',
  //   type: 'num',
  //   grades: {
  //     l: [0, 0, 1, 1],
  //     m: [0, 0, 1, 1],
  //     h: [0, 0, 1, 1]
  //   }
  // },
  // {
  //   _id: 'k5.1',
  //   type: 'num',
  //   grades: {
  //     l: [0, 0, 1, 1],
  //     m: [0, 0, 1, 1],
  //     h: [0, 0, 1, 1]
  //   }
  // },
  // {
  //   _id: 'k5.2',
  //   type: 'num',
  //   grades: {
  //     l: [0, 0, 1, 1],
  //     m: [0, 0, 1, 1],
  //     h: [0, 0, 1, 1]
  //   }
  // },
  // {
  //   _id: 'k5.3',
  //   type: 'num',
  //   grades: {
  //     l: [0, 0, 1, 1]
  //   }
  // },

  //
  {
    _id: 'm2price.0',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.1',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.2',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.3',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300]
    }
  },
//
  {
    _id: 'floorArea.0',
    type: 'num',
    grades: {
      l: [75, 80, 120, 130],
      m: [300, 350, 450, 475],
      h: [375, 450, 550, 600]
    }
  },
  {
    _id: 'floorArea.1',
    type: 'num',
    grades: {
      l: [75, 80, 120, 130],
      m: [375, 450, 550, 625],
      h: [375, 450, 600, 625]
    }
  },
  {
    _id: 'floorArea.2',
    type: 'num',
    grades: {
      l: [55, 60, 100, 110],
      m: [375, 450, 600, 625],
      h: [450, 500, 550, 600]
    }
  },
  {
    _id: 'floorArea.3',
    type: 'num',
    grades: {
      l: [300, 350, 450, 475]
    }
  },
  //

  {
    _id: 'flatsQty.0',
    type: 'range',
    grades: {
      m: [2, 4, 6.5, 8.5],
      h: [2, 3, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.1',
    type: 'range',
    grades: {
      m: [3, 4, 8.5, 8.5],
      h: [4, 6, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.2',
    type: 'range',
    grades: {
      m: [4, 6, 8.5, 8.5],
      h: [3, 4, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.3',
    type: 'range',
    grades: {
      l: [2, 2, 4, 8]
    }
  },

  //

  {
    _id: 'elevatorsQty.0',
    type: 'num',
    grades: {
      m: [1, 1, 2, 4],
      h: [2, 2, 4, 4]
    }
  },
  {
    _id: 'elevatorsQty.1',
    type: 'num',
    grades: {
      m: [1, 2, 3, 4],
      h: [2, 2, 4, 4]
    }
  },
  {
    _id: 'elevatorsQty.2',
    type: 'num',
    grades: {
      m: [1, 2, 3, 4],
      h: [2, 2 ,4, 4]
    }
  },
  {
    _id: 'elevatorsQty.3',
    type: 'num',
    grades: {
      l: [1, 1, 2, 3]
    }
  },

  //  --- --- SUB 3
  {
    _id: 'area.0',
    type: 'num',
    grades: {
      l: [19, 19, 21, 25],
      m: [19, 19, 21, 25],
      h: [19, 19, 21, 25]
    }
  },
  {
    _id: 'area.1',
    type: 'num',
    grades: {
      l: [35, 39, 41, 45],
      m: [35, 39, 41, 45],
      h: [35, 39, 41, 45]
    }
  },
  {
    _id: 'area.2',
    type: 'num',
    grades: {
      l: [55, 59, 61, 65],
      m: [55, 59, 61, 65],
      h: [55, 59, 61, 65]
    }
  },
  {
    _id: 'area.3',
    type: 'num',
    grades: {
      l: [75, 79, 81, 85],
      m: [75, 79, 81, 85],
      h: [75, 79, 81, 85]
    }
  },
  {
    _id: 'area.4',
    type: 'num',
    grades: {
      l: [95, 99, 101, 105],
      m: [95, 99, 101, 105],
      h: [95, 99, 101, 105]
    }
  },

    //
  // {
  //   _id: 'liveArea.0',
  //   type: 'num',
  //   grades: {
  //     l: [13.3, 13.3, 14.7, 17.5],
  //     m: [13.3, 13.3, 14.7, 17.5],
  //     h: [13.3, 13.3, 14.7, 17.5]
  //   }
  // },
  // {
  //   _id: 'liveArea.1',
  //   type: 'num',
  //   grades: {
  //     l: [24.5, 27.3, 28.7, 31.5],
  //     m: [24.5, 27.3, 28.7, 31.5],
  //     h: [24.5, 27.3, 28.7, 31.5]
  //   }
  // },
  // {
  //   _id: 'liveArea.2',
  //   type: 'num',
  //   grades: {
  //     l: [38.5, 41.3, 42.7, 45.5],
  //     m: [38.5, 41.3, 42.7, 45.5],
  //     h: [38.5, 41.3, 42.7, 45.5]
  //   }
  // },
  // {
  //   _id: 'liveArea.3',
  //   type: 'num',
  //   grades: {
  //     l: [52.5, 55.3, 56.7, 59.5],
  //     m: [52.5, 55.3, 56.7, 59.5],
  //     h: [52.5, 55.3, 56.7, 59.5]
  //   }
  // },
  // {
  //   _id: 'liveArea.4',
  //   type: 'num',
  //   grades: {
  //     l: [66.5, 69.3, 70.7, 73.5],
  //     m: [66.5, 69.3, 70.7, 73.5],
  //     h: [66.5, 69.3, 70.7, 73.5]
  //   }
  // },

  // {
  //   _id: 'buildingHeight',
  //   type: 'tri',
  //   grades: {
  //     l: [1, 2, 2, 2],
  //     m: [1, 2, 2, 2],
  //     h: [1, 2, 2, 2]
  //   }
  // },

//   buildingHeight: { type: Number, min: 0 },
// flatArea: { type: Number, min: 0 },
// buildingArea: { type: Number, min: 0 },
// k1: { type: Number, min: 0, max: 100 },
// k2: { type: Number, min: 0, max: 100 },
// k3: { type: Number, min: 0, max: 100 },
// k4: { type: Number, min: 0, max: 100 },
// k5: { type: Number, min: 0, max: 100 },
// m2price: { type: Number, min: 0 },
// floorArea: { type: Number, min: 0 },
// flatsQty: [{ type: Number, min: 0 }], // min and max
//   doubleOriented: { type: Boolean },
// elevatorsQty: { type: Number, min: 0 },
// concierge: { type: Boolean },
// transEntrance: { type: Boolean },
// storage: { type: Boolean },
// sharedSpaces: { type: Boolean }
//   {
//     _id: 'buildingHeight',
//     type: 'num',
//     grades: {
//       l: [45, 50, 80, 90],
//       m: [270, 300, 450, 495],
//       h: [360, 400, 450, 500]
//     }
//   },
// {
//   _id: 'liveArea',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// liveArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// flatArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// buildingArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// baseArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// frontArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// glassArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// k1: { type: Number, min: 0, max: 100 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// k2: { type: Number, min: 0, max: 100 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// k3: { type: Number, min: 0, max: 100 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// k4: { type: Number, min: 0, max: 100 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// k5: { type: Number, min: 0, max: 100 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// m2price: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// celHeight: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// floorArea: { type: Number, min: 0 },
// {
//   _id: 'buildingHeight',
//     type: 'num',
//   grades: {
//   l: [45, 50, 80, 90],
//     m: [270, 300, 450, 495],
//     h: [360, 400, 450, 500]
// }
// },
// flatsQty: [{ type: Number, min: 0 }], // min and max
//   doubleOriented: { type: Boolean },
// drainage: { type: Boolean },
// elevatorsQty: { type: Number, min: 0 },
// concierge: { type: Boolean },
// transEntrance: { type: Boolean },
// storage: { type: Boolean },
// sharedSpaces: { type: Boolean }
//   {}
]

function doCheckers(){
  return new Promise(function(resolve, reject){
    Checker.findOne({})
      .lean()
      .then(function(checker){
        // if(checker){
        //   return Promise.reject()
        // }
        return null
      })
      .then(function(){
        return Checker.remove({}).exec()
      })
      .then(function(){
        return Checker.collection.insert(checkers)
      })
      .then(function(){
        console.info('%d checkers were successfully stored.', checkers.length);
        return null;
      })
      .catch(errHandler)
      .finally(resolve)
  })
}

function getThingDummy(envType, pid, sid){
  var defaultSub1Item = { images:[]};
  var defaultSub2Item = { images:[]};
  var defaultSub3Item = { images:[]};
  return {
    envType,
    commonEvaluationFlat:null,
    commonEvaluationBlock:null,
    commonEvaluationBuilding:null,
    avgCost:null,
    sid: sid,
    pid: pid,
    coverImage:[],
    images:[],
    panel:[],
    album:[],
    sub1:[
      {
        id:'0',
        item:_.cloneDeep(defaultSub1Item)//_.assign(_.cloneDeep(defaultSub1Item), {id:'0'})
      }
    ],
    sub2:[
      {
        id:'0',
        item:_.cloneDeep(defaultSub1Item)//_.assign(_.cloneDeep(defaultSub2Item), { id:'0'})
      },
      {
        id:'1',
        item:_.cloneDeep(defaultSub2Item)//_.assign(_.cloneDeep(defaultSub2Item), { id:'1'})
      },
      {
        id:'2',
        item:_.cloneDeep(defaultSub3Item)//_.assign(_.cloneDeep(defaultSub2Item), { id:'2'})
      }
    ],
    sub3:[
      {
        id:'0',
        item:_.cloneDeep(defaultSub1Item)
      },
      {
        id:'1',
        item:_.cloneDeep(defaultSub1Item)
      },
      {
        id:'2',
        item:_.cloneDeep(defaultSub1Item)
      },
      {
        id:'3',
        item:_.cloneDeep(defaultSub1Item)
      },
      {
        id:'4',
        item:_.cloneDeep(defaultSub1Item)
      }
    ],
  }
}
function doThings() {
  var things = _.map(new Array(200), function (item, index) {
    var envType = _.sample(['l', 'm', 'h']);
    return getThingDummy(envType, envType + '_' + index, secretNumbers[index]);
  })
  return new Promise(function (resolve, reject) {
    Thing.findOne({})
      .lean()
      .then(function (thing) {
        if (thing) {
          return Promise.reject()
        }
        return null
      })
      .then(function () {
        return Thing.create(things)
      })
      .then(function() {
        console.info('%d things were successfully stored.', things.length);
        return null
      })
      .catch(errHandler)
      .finally(resolve)
  })
}

function errHandler(err){
  if(err){
    console.log(err)
  }
}


function doStages() {
  return new Promise(function (resolve, reject) {
    Stage.findOne({})
      .lean()
      .then(function (stage) {
        if (stage) {
          return Promise.reject()
        }
        return null
      })
      .then(function () {
       return Promise.all([
          Stage.create({
            dueDate: new Date().getTime() + 1000 * 60 * 5,
            order:0
          }),
          Stage.create({ order:1 }),
          Stage.create({ order:2 })
        ])
      })
      .then(function(){
        console.info('%d Stages were successfully stored.', 3);
        return null
      })
      .catch(errHandler)
      .finally(resolve)
  })
}
doStages()
  .then(function(){
    return doSystem()
  })
  .then(function(){
    return doUsers()
  })
  .then(function(){
    return doSids()
  })
  .then(function (){
    return doCheckers()
  })
  // .then(function(){
  //   return doThings()
  // })
  .catch(errHandler)
// removeAll3Likes()

//
// function removeAll3Likes(){
//   const thingsToRemove = []
//   return Stage
//     .findOne({ order: 0 })
//     .populate('passedWorks.thing')
//     .then((prevStage) => {
//       return Promise.each(prevStage.passedWorks, (thing) => {
//         if (!thing.thing) {
//           return Promise.resolve()
//         }
//         return Like
//           .find({ thing: thing.thing._id, stage: prevStage._id })
//           .lean()
//           .then((likeList) => {
//             const score = _.sumBy(likeList, 'value')
//             console.log('scorecount', score, thing.thing.pid);
//             if (score ==3) {
//               thingsToRemove.push(thing)
//             }
//             return score
//           })
//       })
//     })
//     .then(() => {
//       console.log('to remove', thingsToRemove.length)
//       Stage
//         .findOneAndUpdate({ order: 0 })
//     })
// }