'use strict'

var express = require('express')
var controller = require('./user.controller')
// var config = require('../../../config').backend
var auth = require('../../auth/auth.service')
var router = express.Router()

const isAdmin = auth.hasRoles(['admin']);
const isVoter = auth.hasRoles(['voter']);

router.get('/', isAdmin, controller.index)
router.delete('/:id', isAdmin, controller.destroy)
router.get('/me', auth.isAuthenticated(), controller.me)
router.get('/me/performance', auth.isAuthenticated(), controller.performance)
router.put('/:id/password', isAdmin, controller.changePassword)
router.get('/:id', isAdmin, controller.show)
router.post('/', isAdmin, controller.create)

module.exports = router
