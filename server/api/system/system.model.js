'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var SystemSchema = new Schema({
  currentStage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stage'
  },
  votingIsAllowed: { type: Boolean, default: false }
})

module.exports = mongoose.model('System', SystemSchema)
