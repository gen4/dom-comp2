/**
 * Broadcast updates to client when the model changes
 */

'use strict'

var _ = require('lodash')

var sockets = []
exports.addSocket = function (socket) {
  sockets.push(socket)
}

exports.removeSocket = function (socket) {
  sockets = _.reject(sockets, socket)
}

// exports.register = function (socket) {
//   console.log('ZA')
//   // system.pre('update', function (doc) {
//   //   console.log('DOC', doc, this)
//   //   stageChanged(socket, doc)
//   //   this.update({},{ $set: { updatedAt: new Date() } });
//   // })
//   system.schema.pre('save', function (next) {
//     console.log('DOC save')
//   })
//   system.schema.pre('update', function (next) {
//     console.log('DOC')
//   })
//
//   system.schema.pre('findOneAndUpdate', function (next) {
//     console.log('DOC+')
//   })
// }

exports.stageChanged = function (data) {
  console.log(' : ', sockets.length);
  _.each(sockets, function (socket) {
    socket.emit('system:stageChanged', data)
  })
}
