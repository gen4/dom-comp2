'use strict'

var express = require('express')
var controller = require('./thing.controller')
var auth = require('../../auth/auth.service')

var router = express.Router()

const isAdmin = auth.hasRoles(['admin']);
const isVoter = auth.hasRoles(['voter']);

router.get('/', auth.isAuthenticated(), controller.index)
router.get('/:id', auth.isAuthenticated(), controller.show)
router.get('/:id/next', auth.isAuthenticated(), controller.showNext)
router.get('/:id/prev', auth.isAuthenticated(), controller.showPrev)
router.post('/', isAdmin, controller.create)
router.post('/:id/like', isVoter, controller.like)
router.put('/:id', isAdmin, controller.update)
router.patch('/:id', isAdmin, controller.update)
router.delete('/:id', isAdmin, controller.destroy)

module.exports = router
