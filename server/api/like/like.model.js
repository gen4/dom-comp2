'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var likeSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  thing: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Thing'
  },
  stage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stage'
  },
  value: { type: Number }
})

// compound index
likeSchema.index({ 'user': 1, 'thing': 1, 'stage': 1 }, { unique: true })

module.exports = mongoose.model('Like', likeSchema)
