/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var Like = require('./like.model')
var handleError = require('../errorhandler')
// Get list of sids
exports.index = function (req, res) {
  Like.find({})
    .lean()
    .then(function (likes) {
      return res.status(200).json({ results: likes })
    })
    .catch(handleError)
}

