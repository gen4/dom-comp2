'use strict'

var express = require('express')
var controller = require('./like.controller')
var auth = require('../../auth/auth.service')
const isAdmin = auth.hasRoles(['admin']);
const isVoter = auth.hasRoles(['voter']);

var router = express.Router()

router.get('/', controller.index)
// router.get('/:id', controller.show)
router.post('/', isVoter, controller.make)
// router.put('/:id', controller.update)
// router.patch('/:id', controller.update)
// router.delete('/:id', controller.destroy)

module.exports = router
