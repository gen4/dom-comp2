'use strict'

var express = require('express')
var auth = require('../../auth/auth.service')
var controller = require('./image.controller')

var router = express.Router()

router.post('/', auth.hasRoles(['admin']), controller.upload, controller.create)
router.put('/', auth.hasRoles(['admin']), controller.upload, controller.create)
router.delete('/:id', auth.hasRoles(['admin']), controller.destroy)

// router.put('/:id', auth.isAuthenticated(), controller.changePassword)

module.exports = router
