'use strict'

var express = require('express')
var controller = require('./stage.controller')
var auth = require('../../auth/auth.service')
const isAdmin = auth.hasRoles(['admin']);
const isVoter = auth.hasRoles(['voter']);

var router = express.Router()

router.get('/', controller.index)
// router.get('/:id', controller.show)
router.post('/', isAdmin, controller.create)
router.post('/setAsCurrent', isAdmin, controller.setAsCurrent)
router.get('/currentThings', controller.getCurrentThings)
router.get('/top100', controller.getTop100)
router.get('/currentLikes', controller.getCurrentLikes)
router.post('/finalizeCurrent', isAdmin, controller.finalizeCurrent)
router.patch('/:id', isAdmin, controller.update)
// router.delete('/:id', controller.destroy)

module.exports = router
