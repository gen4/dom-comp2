'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var winnerSchema = new Schema({
  thing: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Thing'
  },
  score: { type: Number },
  likes: { type: Number }
})
var StageSchema = new Schema({
  dueDate: { type: Date },
  order: { type: Number },
  passedWorks: { type: [winnerSchema], default: [] }
})

module.exports = mongoose.model('Stage', StageSchema)
